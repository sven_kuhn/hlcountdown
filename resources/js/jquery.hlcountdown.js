/* minifyOnSave, filenamePattern: ../../dist/js/$1.min.$2 */

+ function($) {
    'use strict';

    var HlCountdown = function(element, options) {

        var $this = this
        this.options = options
        this.$element = $(element)
        this.eventdatearray = this.options.hlcountdown.split(",")
        this.eventdate = new Date(this.eventdatearray[0], (this.eventdatearray[1] - 1), this.eventdatearray[2], this.eventdatearray[3] || 0, this.eventdatearray[4] || 0).getTime()

        this.$days = this.$element.find(this.options.daysselector)
        this.$hours = this.$element.find(this.options.hoursselector)
        this.$Minutes = this.$element.find(this.options.minutesselector)
        this.$seconds = this.$element.find(this.options.secondsselector)

        this.SetValues(0, 0, 0, 0);

        //$element.find(options.daysselector).html("00");
        //$element.find(options.hoursselector).html("00");
        //$element.find(options.minutesselector).html("00");
        //$element.find(options.secondsselector).html("00");

        var tick2 = setInterval(function() {
                var now = new Date().getTime();
                var remainingtime = this.eventdate - now;

                var days = Math.floor(remainingtime / (1000 * 60 * 60 * 24));
                var hours = Math.floor((remainingtime % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                var minutes = Math.floor((remainingtime % (1000 * 60 * 60)) / (1000 * 60));
                var seconds = Math.floor((remainingtime % (1000 * 60)) / 1000);

                this.SetValues(days, hours, minutes, seconds);

                if (remainingtime < 0) {
                    clearInterval(tick);
                    if (typeof this.options.callback == 'function') {
                        this.options.callback.call(this);
                    } else {
                        this.SetValues(0, 0, 0, 0);
                    }
                }
            }.bind(this),
            1000);

    }

    HlCountdown.prototype.SetValues = function(days, hours, minutes, seconds) {
        this.$element.attr("data-days", ((days < 10) ? "0" : "") + days)
        this.$element.attr("data-hours", ((hours < 10) ? "0" : "") + hours)
        this.$element.attr("data-minutes", ((minutes < 10) ? "0" : "") + minutes)
        this.$element.attr("data-seconds", ((seconds < 10) ? "0" : "") + seconds)

        this.$days.html(((days < 10) ? "0" : "") + days);
        this.$hours.html(((hours < 10) ? "0" : "") + hours);
        this.$Minutes.html(((minutes < 10) ? "0" : "") + minutes);
        this.$seconds.html(((seconds < 10) ? "0" : "") + seconds);
    }


    HlCountdown.VERSION = '0.0.1'

    HlCountdown.DEFAULTS = {
        daysselector: ".days",
        hoursselector: ".hours",
        minutesselector: ".minutes",
        secondsselector: ".seconds",
        callback: false
    }

    function Plugin(option) {
        return this.each(function() {
            var $this = $(this)
            var data = $this.data('hl.countdown')
            var options = $.extend({}, HlCountdown.DEFAULTS, $this.data(), typeof option == 'object' && option)
            if (!data) $this.data('hl.countdown', (data = new HlCountdown(this, options)))
            if (typeof option == 'string') data[option]()
        })
    }

    var old = $.fn.hlcountdown

    $.fn.hlcountdown = Plugin
    $.fn.hlcountdown.Constructor = HlCountdown

    $.fn.hlcountdown.noConflict = function() {
        $.fn.hlcountdown = old
        return this
    }

    $(window).on('load', function() {
        $('[data-hlcountdown]').each(function() {
            var $element = $(this)
            Plugin.call($element, $element.data())
        })
    })
}(jQuery);
